FROM debian:jessie
# Disable debian interactive messages
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
# Azure CLI Prerequisities + zip
RUN apt-get update -qq && \
    apt-get install -qqy --no-install-recommends \
    apt-transport-https lsb-release software-properties-common dirmngr \ 
    zip \
    && \
    rm -rf /var/lib/apt/lists/*
# Install Azure CLI
RUN AZ_REPO=$(lsb_release -cs) && \
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    tee /etc/apt/sources.list.d/azure-cli.list && \
    apt-key --keyring /etc/apt/trusted.gpg.d/Microsoft.gpg adv \
     --keyserver packages.microsoft.com \
     --recv-keys BC528686B50D79E339D3721CEB3E94ADBE1229CF \
    && \    
    apt-get update && \
    apt-get install -qqy --no-install-recommends azure-cli && \
    rm -rf /var/lib/apt/lists/*