# Azure-cli-zip

This is just a repo to generate a docker image with the azure-cli tool and zip command.

* See [Dockerfile]
* Image: registry.gitlab.com/kragefolket/azure-cli-zip:latest
